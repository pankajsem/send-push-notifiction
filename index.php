<?php
require_once './vendor/autoload.php';

define('API_ACCESS_KEY', "YOUR SERVER KEY"); //firebase api server key.
define('DEVICE_TOKEN', "YOUR DEVICE TOKEN"); //The device token.

use sngrl\PhpFirebaseCloudMessaging\Client;
use sngrl\PhpFirebaseCloudMessaging\Message;
use sngrl\PhpFirebaseCloudMessaging\Notification;
use sngrl\PhpFirebaseCloudMessaging\Recipient\Device;
/**
 * Class Push
 */
class Push
{
    public $server_key;
    /**
     * Class constructor
     */
    public function __construct($server_key)
    {
        $this->server_key = $server_key;
    }
    
    /**
     * @var SendPushNotifiction
     * @param string $title,
     * @param string $messageText,
     * @return boolean true OR false
     */

    public function sendPushNotification($token, $title, $messageText)
    {
        $client = new Client();
        $client->setApiKey($this->server_key);
        $client->injectGuzzleHttpClient(new \GuzzleHttp\Client());
        $message = new Message();
        $message->setPriority('high');
        $message->addRecipient(new Device($token));
        $message->setNotification(new Notification($title, $messageText));
        $response = $client->send($message);
        return $response;
    }
}

$pushObj = new Push(API_ACCESS_KEY);
$title = "Test title";
$messageText = "Test title";
$response = $pushObj->sendPushNotification(DEVICE_TOKEN, $title, $messageText);
var_dump($response);