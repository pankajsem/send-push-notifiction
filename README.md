**PHP API for Firebase Cloud Messaging from Google**

Or add this to your composer.json and run "composer update":

```
"require": {
    "sngrl/php-firebase-cloud-messaging": "dev-master"
}
```
Open index.php and replace your credentials
```
define('API_ACCESS_KEY', "YOUR SERVER KEY"); //firebase api server key.
define('DEVICE_TOKEN', "YOUR DEVICE TOKEN"); //The device token.
```
